# JenkinsLab

Lab for work with Jenkins environment

```mermaid
sequenceDiagram
Jenkins Master -> Jenkins Node: SSH ready
Jenkins Node -->> Target Server: Ansible core ready
Jenkins Node -> Jenkins Master: Jenkins Node with docker support
```

Do not use for production purposes!

Thanks to: culturadevops
