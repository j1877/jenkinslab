Vagrant.configure("2") do |config|

    config.vm.define "master" do |master|
        master.vm.hostname = "masterjenkins"
        master.vm.box = "almalinux/8"
        master.vm.box_check_update = false

        master.vm.provision "shell", inline: <<-SHELL
            sudo yum -y install vim wget git
            sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
            sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
            sudo yum -y upgrade
            sudo yum -y install java-11-openjdk
            sudo yum -y install jenkins
            sudo systemctl daemon-reload
            sudo systemctl enable --now jenkins
            sudo cat /var/lib/jenkins/secrets/initialAdminPassword
            echo -e "192.168.56.5\\tslave1" >> /etc/hosts
            echo -e "192.168.56.6\\tslave2" >> /etc/hosts
        SHELL
        master.vm.network "private_network", ip: "192.168.56.4"
        master.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.1", id: "jenkins"

        master.vm.provider "virtualbox" do |vb|
            # Display the VirtualBox GUI when booting the machine
            vb.gui = false

            # Customize the amount of memory on the VM:
            vb.memory = "4096"
            vb.customize ["modifyvm", :id, "--vram", "128"]
            vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
        end
    end

    config.vm.define "slave1" do |slave1|
        slave1.vm.hostname = "slavejenkins"
        slave1.vm.box = "almalinux/8"
        slave1.vm.box_check_update = false

        slave1.vm.provision "shell", inline: <<-SHELL
            sudo yum -y install java-11-openjdk git python3-virtualenv rsync
            sudo useradd -d /var/lib/jenkins jenkins
            sudo mkdir /var/lib/jenkins/.ssh
            sudo cp /vagrant/keys/jenkins /var/lib/jenkins/.ssh/id_rsa
            sudo cat /vagrant/keys/jenkins.pub > /var/lib/jenkins/.ssh/authorized_keys
            virtualenv /opt/pyenv/p3/ansible
            source /opt/pyenv/p3/ansible/bin/activate && pip install ansible==2.9.5 jmespath yamllint
            sudo chown jenkins. -R /var/lib/jenkins/ /opt/pyenv/p3/ansible/
            sudo sed -Ei "s/^#?PubkeyAuthentication.*/PubkeyAuthentication yes/g" /etc/ssh/sshd_config
            sudo systemctl restart sshd
            echo -e "192.168.56.6\\tslave2" >> /etc/hosts
        SHELL

        slave1.vm.provision "shell", inline: <<-SHELL
            sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
            sudo yum install -y yum-utils
            sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
            sudo yum install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
            sudo usermod -aG docker jenkins
            sudo systemctl enable --now docker
        SHELL

        slave1.vm.network "private_network", ip: "192.168.56.5"
        slave1.vm.network "forwarded_port", guest: 80, host: 8000, host_ip: "127.0.0.1", id: "apache"

        slave1.vm.provider "virtualbox" do |vb|
            # Display the VirtualBox GUI when booting the machine
            vb.gui = false

            # Customize the amount of memory on the VM:
            vb.memory = "2048"
            vb.customize ["modifyvm", :id, "--vram", "128"]
            vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
        end
    end

    config.vm.define "slave2" do |slave2|
        slave2.vm.hostname = "targetjenkins"
        slave2.vm.box = "almalinux/8"
        slave2.vm.box_check_update = false

        slave2.vm.provision "shell", inline: <<-SHELL
            sudo cat /vagrant/keys/jenkins.pub > /home/vagrant/.ssh/authorized_keys
            sudo chown vagrant. -R /home/vagrant/
            sudo sed -Ei "s/^#?PubkeyAuthentication.*/PubkeyAuthentication yes/g" /etc/ssh/sshd_config
            sudo systemctl restart sshd
        SHELL

        slave2.vm.network "private_network", ip: "192.168.56.6"

        slave2.vm.provider "virtualbox" do |vb|
            # Display the VirtualBox GUI when booting the machine
            vb.gui = false

            # Customize the amount of memory on the VM:
            vb.memory = "1024"
            vb.customize ["modifyvm", :id, "--vram", "128"]
            vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
        end
    end

    config.vm.synced_folder "./", "/vagrant", disabled: false

  end
